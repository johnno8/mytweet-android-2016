package org.wit.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void loginApplyButtonPressed(View view)
    {
        MyTweetApp app = (MyTweetApp) getApplication();

        TextView email = (TextView) findViewById(R.id.loginEmail);
        TextView password = (TextView) findViewById(R.id.loginPassword);

        if(app.validUser(email.getText().toString(), password.getText().toString()))
        {
            startActivity(new Intent(this, TimelineActivity.class));
        }
        else
        {
            Toast toast = Toast.makeText(this, "Invalid credentials", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
