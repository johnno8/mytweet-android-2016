package org.wit.mytweet.activities;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.android.helpers.ContactHelper;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweetlist;

import java.util.Date;

import static org.wit.mytweet.android.helpers.ContactHelper.sendEmail;
import static org.wit.mytweet.android.helpers.IntentHelper.navigateUp;

/**
 * Created by John on 21/10/2016.
 */
public class TweetFragment extends Fragment implements TextWatcher, View.OnClickListener
{

    private TextView characterCount;
    private TextView dateView;
    private EditText messageView;
    private Tweet tweet;
    private Button tweetButton;
    private Button contactButton;
    private Button emailButton;
    private Tweetlist tweetlist;
    MyTweetApp app;
    private static final int REQUEST_CONTACT = 1;
    private String emailAddress = "";
    private Long tweetId;
    // This field is initialized in `onActivityResult`.
    Intent data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        tweetId = (Long) getActivity().getIntent().getSerializableExtra("TWEET_ID");
        app = MyTweetApp.getApp();
        tweetlist = app.tweetlist;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        super.onCreateView(inflater,  parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

        TweetActivity tweetActivity = (TweetActivity)getActivity();
        tweetActivity.actionBar.setDefaultDisplayHomeAsUpEnabled(true);

        dateView = (TextView) v.findViewById(R.id.mytweetDate);
        Long date = new Date().getTime();

        tweetButton = (Button) v.findViewById(R.id.tweetButton);
        tweetButton.setOnClickListener(this);

        contactButton = (Button)v.findViewById(R.id.mytweetContactButton);
        contactButton.setOnClickListener(this);

        emailButton = (Button) v.findViewById(R.id.mytweetEmailButton);
        emailButton.setOnClickListener(this);

        String dateFormat = "MMM d, yyyy H:mm:ss";
        dateView.setText(android.text.format.DateFormat.format(dateFormat, date));

        characterCount = (TextView) v.findViewById(R.id.numCharacters);
        messageView = (EditText) v.findViewById(R.id.mytweetMessage);

        messageView.addTextChangedListener(this);

        tweetlist = app.tweetlist;
        {
            if (tweetId != null) {
                tweet = tweetlist.getTweet(tweetId);

                updateControls(tweet);
            }
        }

        return v;
    }

    public void updateControls(Tweet tweet)
    {
        messageView.setText(tweet.tweetText);
        dateView.setText(tweet.tweetGenerationDate);
        messageView.setEnabled(false);
        tweetButton.setEnabled(false);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        final int ALLOWED_CHARACTERS = 140;
        characterCount.setText(String.valueOf(ALLOWED_CHARACTERS - (s.length())));
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:  navigateUp(getActivity());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        tweetlist.saveTweets();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.tweetButton:
                String contents = messageView.getText().toString();
                if (!contents.equals("")) {
                    tweet = new Tweet(contents);
                    app.tweetlist.newTweet(tweet);
                    messageView.setText("");
                    Toast toast = Toast.makeText(getActivity(), "Message Sent", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    Toast toast = Toast.makeText(getActivity(), "Cannot send an empty tweet", Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;

            case R.id.mytweetContactButton:
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                break;

            case R.id.mytweetEmailButton:
                if(emailAddress == null) emailAddress = ""; // guard against null pointer
                sendEmail(getActivity(), emailAddress, getString(R.string.emailSubject), messageView.getText().toString());
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            case REQUEST_CONTACT:
                this.data = data;
                checkContactsReadPermission();
                break;
        }
    }

    private void readContact()
    {
        String name = ContactHelper.getContact(getActivity(), data);
        emailAddress = ContactHelper.getEmail(getActivity(), data);
        contactButton.setText(name + " : " + emailAddress);
    }

    /**
     * http://stackoverflow.com/questions/32714787/android-m-permissions-onrequestpermissionsresult-not-being-called
     * This is an override of FragmentCompat.onRequestPermissionsResult
     *
     * @param requestCode Example REQUEST_CONTACT
     * @param permissions String array of permissions requested.
     * @param grantResults int array of results for permissions request.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    readContact();
                }
                break;
            }
        }
    }

    /**
     * Bespoke method to check if read contacts permission exists.
     * If it exists then the contact sought is read.
     * Otherwise, the method FragmentCompat.request permissions is invoked and
     * The response is via the callback onRequestPermissionsResult.
     * In onRequestPermissionsResult, on successfully being granted permission then the sought contact is read.
     */
    private void checkContactsReadPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            readContact();
        }
        else {
            // Invoke callback to request user-granted permission
            FragmentCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_CONTACT);
        }
    }
}
