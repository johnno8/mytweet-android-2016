package org.wit.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import org.wit.mytweet.R;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);


    }

    public void signupPressed(View view)
    {
        startActivity(new Intent(this, SignupActivity.class));
    }

    public void loginPressed(View view)
    {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
