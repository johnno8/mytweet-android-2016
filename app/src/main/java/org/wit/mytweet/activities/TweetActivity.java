package org.wit.mytweet.activities;

import org.wit.mytweet.R;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
//import android.support.v4.app.Fragment;
import android.app.Fragment;
import android.app.FragmentManager;
//import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;



public class TweetActivity extends AppCompatActivity
{
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);

        actionBar = getSupportActionBar();

        FragmentManager manager = getFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);
        if (fragment == null)
        {
            fragment = new TweetFragment();
            manager.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
        }
    }
}
