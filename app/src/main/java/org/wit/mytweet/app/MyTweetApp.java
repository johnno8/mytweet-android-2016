package org.wit.mytweet.app;

import android.app.Application;
import android.util.Log;

import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweetlist;
import org.wit.mytweet.models.TweetlistSerializer;
import org.wit.mytweet.models.User;
import static org.wit.mytweet.android.helpers.LogHelpers.info;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 05/10/2016.
 */
public class MyTweetApp extends Application
{
    public List<User> users = new ArrayList<User>();
    public Tweetlist tweetlist;
    protected static MyTweetApp app;
    private static final String FILENAME = "tweetlist.json";

    @Override
    public void onCreate()
    {
        super.onCreate();
        TweetlistSerializer serializer = new TweetlistSerializer(this, FILENAME);
        tweetlist = new Tweetlist(serializer);
        app = this;
        info(this, "MyRent app launched");
    }

    public void newUser(User user)
    {
        users.add(user);
    }

    public boolean validUser(String email, String password)
    {
        for(User user : users)
        {
            if(user.email.equals(email) && user.password.equals(password))
            {
                return true;
            }
        }
        return false;
    }

    public static MyTweetApp getApp(){
        return app;
    }
}
