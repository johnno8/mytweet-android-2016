package org.wit.mytweet.models;



import java.util.ArrayList;
import static org.wit.mytweet.android.helpers.LogHelpers.info;

/**
 * Created by John on 10/10/2016.
 */
public class Tweetlist
{
    public ArrayList<Tweet> tweets;
    private TweetlistSerializer serializer;

    /*public Tweetlist()
    {
        tweets = new ArrayList<Tweet>();
    }*/

    public Tweetlist(TweetlistSerializer serializer)
    {
        this.serializer = serializer;
        try
        {
            tweets = serializer.loadTweets();
        }
        catch (Exception e)
        {
            info(this, "Error loading tweets: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
        }
    }

    public boolean saveTweets()
    {
        try
        {
            serializer.saveTweets(tweets);
            info(this, "Tweets saved to file");
            return true;
        }
        catch (Exception e)
        {
            info(this, "Error saving tweets: " + e.getMessage());
            return false;
        }
    }

    public void newTweet(Tweet tweet)
    {
        tweets.add(tweet);
    }

    public Tweet getTweet(Long id)
    {
        for(Tweet t : tweets)
        {
            if(id.equals(t.id))
            {
                return t;
            }
        }
        return null;
    }

    public int getSize()
    {
        return tweets.size();
    }

    public void clearTimeline()
    {
        tweets.clear();
    }

    public void deleteTweet(Tweet tweet)
    {
        tweets.remove(tweet);
        saveTweets();
    }
}
