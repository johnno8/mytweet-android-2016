package org.wit.mytweet.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by John on 07/10/2016.
 */
public class Tweet
{
    public Long id;
    public String tweetText;
    public String tweetGenerationDate;

    private static final String JSON_ID             = "id"            ;
    private static final String JSON_TWEETEXT   = "tweetText"   ;
    private static final String JSON_TWEETGENERATIONDATE           = "tweetGenerationDate"          ;

    public Tweet(String tweetText)
    {
        this.id = unsignedLong();
        this.tweetText = tweetText;
        this.tweetGenerationDate = new SimpleDateFormat("MMM d, yyyy H:mm:ss").format(new Date());
    }

    public String getTweetText()
    {
        return tweetText;
    }

    /**
     * Generate a long greater than zero
     * @return Unsigned Long value greater than zero
     */
    private Long unsignedLong() {
        long rndVal = 0;
        do {
            rndVal = new Random().nextLong();
        } while (rndVal <= 0);
        return rndVal;
    }

    public Tweet(JSONObject json) throws JSONException
    {
        id            = json.getLong(JSON_ID);
        tweetText   = json.getString(JSON_TWEETEXT);
        tweetGenerationDate          = json.getString(JSON_TWEETGENERATIONDATE);
        //rented        = json.getBoolean(JSON_RENTED);
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID            , Long.toString(id));
        json.put(JSON_TWEETEXT   , tweetText);
        json.put(JSON_TWEETGENERATIONDATE          , tweetGenerationDate);
        //json.put(JSON_RENTED        , rented);
        return json;
    }
}
