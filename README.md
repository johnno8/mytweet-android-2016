# README #

MyTweet is a microblogging app for the Android OS. It was developed incrementally in Android Studio with improved functionality added over time. It allows a user to register, log in, post and delete tweets, view a list of their own tweets in a timeline and email the contents of a tweet to an address from their contact list.
